FROM node:alpine as builder 
# as builder means that everything under FROM 
# will be the BUILDER PHASE
WORKDIR './app' 
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

FROM nginx
# the beginning of RUN PHASE
COPY --from=builder /app/build /usr/share/nginx/html
# copy everything from builder and the build dir
# to that newly defined dir
# everything is detailed in nginx's docker hub docs